//
//  CellView.swift
//  GameOfLife
//
//  Created by Dwayne Coussement on 01/02/2022.
//

import SwiftUI

struct CellView: View {
    let cell: Cell
    
    var body: some View {
        Text(cell.alive == true ? "A" : "D")
            .background(cell.alive == true ? .green : .red)
            .frame(width: 20.0, height: 20.0)
            .cornerRadius(4.0)
    }
}
