//
//  GameOfLifeApp.swift
//  GameOfLife
//
//  Created by Dwayne Coussement on 01/02/2022.
//

import SwiftUI

@main
struct GameOfLifeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(viewModel: .init())
        }
    }
}
