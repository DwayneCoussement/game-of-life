//
//  ContentViewModel.swift
//  GameOfLife
//
//  Created by Dwayne Coussement on 01/02/2022.
//

import Combine
import Foundation

class ContentViewModel: ObservableObject {
    let grid = Grid(columns: 10, rows: 10)
    @Published var cells: [Cell] = []
    
    private var cancellables = Set<AnyCancellable>()
    
    init() {
        generateCells()
    }
    
    private func generateCells() {
        var cells = [Cell]()
        for column in 0..<grid.columns {
            for row in 0..<grid.rows {
                let cell = Cell(column: column, row: row, alive: Bool.random())
                cells.append(cell)
            }
        }
        self.cells = cells
    }
    
    private var timer = Timer.publish(every: 0.5, on: .main, in: .common).autoconnect()
    private var counter = 0

    func startGame() {
        setupTimer()
        timer.sink { [weak self] _ in
            guard let self = self else { return }
            if self.counter == 3 {
                self.timer.upstream.connect().cancel()
            } else {
                self.cells = self.cells.map { self.generation(for: $0) }
            }
            self.counter += 1
        }.store(in: &cancellables)
    }
    
    private func setupTimer() {
        self.timer = Timer.publish(every: 0.5, on: .main, in: .common).autoconnect()
        counter = 0
    }
    
    func generation(for cell: Cell) -> Cell {
        var cell = cell
        let neighbors = cell.neighbors(in: grid)
        
        let neighborsAlive: [Cell] = neighbors.compactMap { neighbor in
            let cell = self.cell(column: neighbor.column, row: neighbor.row)
            if cell?.alive == true {
                return cell
            } else {
                return nil
            }
        }
                
        if cell.alive == false && neighborsAlive.count == 3 {
            cell.alive = true
        } else if cell.alive == true && neighborsAlive.count < 2 {
            cell.alive = false
        } else if cell.alive == true && (neighborsAlive.count == 2 || neighborsAlive.count == 3) {
            cell.alive = true
        } else if cell.alive == true && neighborsAlive.count > 3 {
            cell.alive = false
        } else {
            cell.alive = false
        }
        
        return cell
    }
    
    func cell(column: Int, row: Int) -> Cell! {
        cells.first { $0.column == column && $0.row == row }
    }
}
