//
//  Cell.swift
//  GameOfLife
//
//  Created by Dwayne Coussement on 01/02/2022.
//

import Foundation

struct Cell: Equatable {
    let column: Int
    let row: Int
    var alive: Bool?
}

extension Cell {
    func neighbors(in grid: Grid) -> [Cell] {
        unvalidatedNeighors.filter { neighbor in
            grid.isValid(cell: neighbor)
        }
    }
    
    var unvalidatedNeighors: [Cell] {
        let top = Cell(column: column, row: row - 1)
        let bottom = Cell(column: column, row: row + 1)
        let leading = Cell(column: column - 1, row: row)
        let trailing = Cell(column: column + 1, row: row)
        let topLeading = Cell(column: column - 1, row: row - 1)
        let topTrailing = Cell(column: column + 1, row: row - 1)
        let bottomLeading = Cell(column: column - 1, row: row + 1)
        let bottomTrailing = Cell(column: column + 1, row: row + 1)
        return [top, bottom, leading, trailing, topLeading, topTrailing, bottomLeading, bottomTrailing]
    }
}
