//
//  Grid.swift
//  GameOfLife
//
//  Created by Dwayne Coussement on 01/02/2022.
//

import Foundation

struct Grid {
    let columns: Int
    let rows: Int
    
    func isValid(cell: Cell) -> Bool {
        if cell.column > columns-1 || cell.column < 0 {
            return false
        }
        if cell.row > rows-1 || cell.row < 0 {
            return false
        }
        return true
    }
}
