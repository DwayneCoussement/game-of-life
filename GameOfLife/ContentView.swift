//
//  ContentView.swift
//  GameOfLife
//
//  Created by Dwayne Coussement on 01/02/2022.
//

import SwiftUI

struct ContentView: View {
    @StateObject var viewModel: ContentViewModel
    
    var body: some View {
        VStack {
            HStack(alignment: .top, spacing: .zero) {
                ForEach(0..<self.viewModel.grid.columns, id: \.self) { column in
                    VStack(alignment: .leading, spacing: .zero) {
                        ForEach(0..<self.viewModel.grid.rows, id: \.self) { row in
                            CellView(cell: viewModel.cell(column: column, row: row))
                        }
                    }
                }
            }
            Button(action: {
                viewModel.startGame()
            }) {
                Text("Start")
            }
        }
    }
}
