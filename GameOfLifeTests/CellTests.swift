//
//  CellTests.swift
//  GameOfLifeTests
//
//  Created by Dwayne Coussement on 01/02/2022.
//

import XCTest
@testable import GameOfLife

class CellTests: XCTestCase {
    func testNeightbors() {
        let grid = Grid(columns: 2, rows: 2)
        let cell = Cell(column: 0, row: 1)
        let neighbors = cell.neighbors(in: grid)
        XCTAssertEqual(neighbors, [
            Cell(column: 0, row: 0),
            Cell(column: 1, row: 1),
            Cell(column: 1, row: 0)
        ])
    }
}
