//
//  GridTests.swift
//  GameOfLifeTests
//
//  Created by Dwayne Coussement on 01/02/2022.
//

import XCTest
@testable import GameOfLife

class GridTests: XCTestCase {
    func testOutOfBounds() {
        let grid = Grid(columns: 2, rows: 2)
        XCTAssertFalse(grid.isValid(cell: Cell(column: 0, row: -1, alive: false)))
        XCTAssertFalse(grid.isValid(cell: Cell(column: -1, row: 0, alive: false)))
        XCTAssertFalse(grid.isValid(cell: Cell(column: -1, row: -1, alive: false)))
        XCTAssertFalse(grid.isValid(cell: Cell(column: 2, row: 0, alive: false)))
        XCTAssertFalse(grid.isValid(cell: Cell(column: 0, row: 2, alive: false)))
    }
    
    func testValid() {
        let grid = Grid(columns: 2, rows: 2)
        XCTAssertTrue(grid.isValid(cell: Cell(column: 0, row: 0, alive: false)))
        XCTAssertTrue(grid.isValid(cell: Cell(column: 1, row: 0, alive: false)))
        XCTAssertTrue(grid.isValid(cell: Cell(column: 1, row: 1, alive: false)))
        XCTAssertTrue(grid.isValid(cell: Cell(column: 0, row: 1, alive: false)))
    }
}
